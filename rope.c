#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#define NUM_POINTS 50
#define NUM_LINES 50

struct vector{
	float x;
	float y;
};

//will represent a point in our physics simulation, prev_position is used to keep track of velocity
struct point{
	struct vector position;
	struct vector prev_position;
	bool locked;
	bool exists;
};

//will represent a connection between 2 points in the physics simulation, the points will be kept at length distance apart
struct line{
	struct point *a;
	struct point *b;
	float length;
	bool exists;
};

//sleeps for some number of milliseconds
int msleep(long tms){
	struct timespec ts;
	int ret;
	if (tms < 0)
	{
		return -1;
	}
	ts.tv_sec = tms / 1000;
	ts.tv_nsec = (tms % 1000) * 1000000;
	do {
		ret = nanosleep(&ts, &ts);
	} while (ret);
	return ret;
}

//returns the square of the input
float square(int x){
	return x*x;
}

int maxRow;
int maxCol;
const int frameDelay = 60; // minimum number of milliseconds in between frames
const float GRAVITY = 0.1;
const float LINE_LEN_MULT = 0.5; // affects how stretchy the lines are
int main(){
	//initialize some stuff for ncurses
	initscr();
	noecho();
	curs_set(0);
	MEVENT mEvent;
	mousemask(BUTTON1_CLICKED, 0);
	keypad(stdscr, TRUE);
	timeout(frameDelay); // getch() will wait frameDelay before moving on
	getmaxyx(stdscr, maxRow, maxCol);
	//initialize arrays for the points and lines
	struct point points[NUM_POINTS];
	struct line lines[NUM_LINES];
	for(int i = 0; i < NUM_POINTS; i++){
		points[i].exists = false;
		points[i].position.x = 0;
		points[i].position.y = 0;
		points[i].prev_position.x = 0;
		points[i].prev_position.y = 0;
	}
	for(int i = 0; i < NUM_LINES; i++){
		lines[i].exists = false;
		lines[i].length = 0.0;
		lines[i].a = NULL;
		lines[i].b = NULL;
	}
	while(1){ // simulation loop
		//use clock to wait the remaining time, keeping the minimum frame delay
		clock_t start, end;
		start = clock();
		char input = getch();
		end = clock();
		unsigned long delta = ((end - start) * 1000) / CLOCKS_PER_SEC; // delta in ms
		if(frameDelay-delta > 0){
			msleep(frameDelay-delta);
		}
		if(input == 'q'){//quits the simulation
			break;
		}else if(input == 'p'){//adds locked point at clicked location
			timeout(-1);
			mvprintw(maxRow-1, 0, "p: click somewhere to add a point");
			refresh();
			input = getch();
			if(getmouse(&mEvent) == OK){
				int i = 0;
				while(i < NUM_POINTS && points[i].exists){
					i++;
				}
				if(i < NUM_POINTS){
					//there is room in the array
					points[i].exists = true;
					points[i].locked = true;
					points[i].position.x = (float)mEvent.x;
					points[i].position.y = (float)mEvent.y;
					points[i].prev_position = points[i].position;
				}else{
					//there isnt room
					mvprintw(maxRow-1, 0, "ERROR: TOO MANY POINTS                     ");
					refresh();
					msleep(500);
				}
			}
			timeout(frameDelay);
		}else if(input == 'u'){//toggle lock of clicked point
			timeout(-1);
			mvprintw(maxRow-1, 0, "u: click on a point to toggle its lock");
			refresh();
			input = getch();
			if(getmouse(&mEvent) == OK){
				for(int i = 0; i < NUM_POINTS; i++){
					if((int)(points[i].position.x) == mEvent.x && (int)(points[i].position.y) == mEvent.y){
						points[i].locked = !points[i].locked;
					}
				}
			}
			timeout(frameDelay);
		}else if(input == 'U'){ // let user select a rectangle to unlock points in
			timeout(-1);
			mvprintw(maxRow-1, 0, "U: select a corner of a rectangular area to unlock");
			refresh();
			struct vector a;
			a.x = -1;
			a.y = -1;
			struct vector b;
			b.x = -1;
			b.y = -1;
			input = getch();
			if(getmouse(&mEvent) == OK){
				a.x = mEvent.x;
				a.y = mEvent.y;
			}
			if(a.x >= 0 && a.y >= 0){
				mvprintw(maxRow-1, 0, "U: select the other corner of a rectangular area to unlock");
				refresh();
				input = getch();
				if(getmouse(&mEvent) == OK){
					b.x = mEvent.x;
					b.y = mEvent.y;
				}
				if(b.x >= 0 && b.y >= 0){
					//make sure b is smaller than a in both x and y
					if(b.x < a.x){
						int tmp = b.x;
						b.x = a.x;
						a.x = tmp;
					}
					if(b.y < a.y){
						int tmp = b.y;
						b.y = a.y;
						a.y = tmp;
					}
					//unlock all the points in this range
					for(int i = 0; i < NUM_POINTS; i++){
						if(points[i].exists && (int)(points[i].position.x) > a.x && (int)(points[i].position.x) < b.x && (int)(points[i].position.y) > a.y && (int)(points[i].position.y) < b.y){
							points[i].locked = false;
						}
					}
				}
			}
			timeout(frameDelay);
		}else if(input == 'l'){//connect 2 points with a line
			timeout(-1);
			mvprintw(maxRow-1, 0, "l: click on a point to start a line");
			refresh();
			input = getch();
			struct line newLine; // create the line that will be added
			newLine.a = NULL;
			newLine.b = NULL;
			if(getmouse(&mEvent) == OK){
				for(int i = 0; i < NUM_POINTS; i++){
					if((int)(points[i].position.x) == mEvent.x && (int)(points[i].position.y) == mEvent.y){
						newLine.a = &points[i];
						break;
					}
				}
			}
			if(newLine.a != NULL){
				mvprintw(maxRow-1, 0, "l: click on another point to end the line");
				refresh();
				input = getch();
				if(getmouse(&mEvent) == OK){
					for(int i = 0; i < NUM_POINTS; i++){
						if((int)(points[i].position.x) == mEvent.x && (int)(points[i].position.y) == mEvent.y){
							newLine.b = &points[i];
							break;
						}
					}
				}
				if(newLine.b != NULL){
					newLine.length = LINE_LEN_MULT*sqrt(square(newLine.a->position.x-newLine.b->position.x)+square(newLine.a->position.y-newLine.b->position.y));
					newLine.exists = true;
					//find the first unused slot in the array
					int i = 0;
					while(i < NUM_LINES && lines[i].exists){
							i++;
					}
					if(i < NUM_LINES){
						//there is room in the line array
						lines[i] = newLine;
					}else{
						//there isnt room
						mvprintw(maxRow-1, 0, "ERROR: TOO MANY LINES                       ");
						refresh();
						msleep(500);
					}
				}
			}
			timeout(frameDelay);	
		}else if(input == 'L'){ // make some points and connect them
			timeout(-1);
			struct point* new[50];
			for(int i = 0; i < 50; i++){
				new[i] = NULL;
			}
			for(int j = 0; j < 50; j++){
				mvprintw(maxRow-1, 0, "L: click somewhere to draw a line, press any key to exit");
				refresh();
				input = getch();
				if(getmouse(&mEvent) == OK){
					int i = 0;
					while(i < NUM_POINTS && points[i].exists){
						i++;
					}
					if(i < NUM_POINTS){
						//there is room in the array
						points[i].exists = true;
						points[i].locked = true;
						points[i].position.x = (float)mEvent.x;
						points[i].position.y = (float)mEvent.y;
						points[i].prev_position = points[i].position;
						new[j] = &points[i];
						attron(A_REVERSE);
						mvprintw(mEvent.y, mEvent.x, "0");
						attroff(A_REVERSE);
					}else{
						//there isnt room
						mvprintw(maxRow-1, 0, "ERROR: TOO MANY POINTS                     ");
						refresh();
						msleep(500);
						break;
					}
				}else{
					//user put something other than a mouse click, exit
					break;
				}
			}
			for(int j = 0; j < 50-1; j++){
				if(new[j] != NULL && new[j+1] != NULL){
					//make a line between these 2 points
					int i = 0;
					while(i < NUM_LINES && lines[i].exists){
						i++;
					}
					if(i < NUM_LINES){
						lines[i].exists = true;
						lines[i].length = LINE_LEN_MULT*sqrt(square(new[j]->position.x-new[j+1]->position.x)+square(new[j]->position.y-new[j+1]->position.y));
						lines[i].a = new[j];
						lines[i].b = new[j+1];
					}
				}else{
					//we reached the end, exit
					break;
				}
			}
			timeout(frameDelay);
		}else if(input == 'v'){ //add to the velocity of a point
			timeout(-1);
			mvprintw(maxRow-1, 0, "v: click on a point to add to its velocity");
			refresh();
			input = getch();
			struct point *p = NULL;
			if(getmouse(&mEvent) == OK){
				for(int i = 0; i < NUM_POINTS; i++){
					if((int)(points[i].position.x) == mEvent.x && (int)(points[i].position.y) == mEvent.y){
						p = &points[i];
						break;
					}
				}
			}
			if(p != NULL){
				mvprintw(maxRow-1, 0, "v: click where you want it to go           ");
				refresh();
				input = getch();
				if(getmouse(&mEvent) == OK){
					p->prev_position.x += ((mEvent.x-p->position.x)*-1);
					p->prev_position.y += ((mEvent.y-p->position.y)*-1);
				}
			}
			timeout(frameDelay);
		}else if(input == 'd'){ // delete a point
			timeout(-1);
			mvprintw(maxRow-1, 0, "d: click on a point to delete it.");
			refresh();
			input = getch();
			if(getmouse(&mEvent) == OK){
				for(int i = 0; i < NUM_POINTS; i++){
					if((int)(points[i].position.x) == mEvent.x && (int)(points[i].position.y) == mEvent.y){
						points[i].exists = false;
					}
				}
			}
			timeout(frameDelay);
		}
		clear();
		//simulate physics for points
		for(int i = 0; i < NUM_POINTS; i++){
			if(!points[i].exists){
				//the point doesnt exist, go to the next one
				continue;
			}
			if(!points[i].locked){
				//point isnt locked in place, simulate physics
				struct vector prev = points[i].position; // keep track of the points previous position
				points[i].position.y += GRAVITY; // add gravity
				points[i].position.x += points[i].position.x-points[i].prev_position.x; // add the difference between the previous position and the current one, keeping the inertia
				points[i].position.y += points[i].position.y-points[i].prev_position.y;
				points[i].prev_position = prev; // keep the previous position to use later
			}
			if(points[i].position.x < -maxCol || points[i].position.x > maxCol*2 || points[i].position.y < -maxRow || points[i].position.y > maxRow*2){
				//delete the point if its out of bounds
				points[i].exists = false;
			}
		}
		//constrain points to lines
		for(int c = 0; c < 100; c++){ // many iterations are required to settle into a stable configuration
			for(int i = 0; i < NUM_LINES; i++){
				if(!lines[i].exists){
					//the line doesnt exist, continue
					continue;
				}
				//check if either of the 2 points doesnt exist
				if(!lines[i].a->exists || !lines[i].b->exists){
					//if it is out of bounds, delete the line and continue
					lines[i].exists = false;
					continue;
				}
				//get the midpoint of this line
				struct vector midpoint;
				midpoint.x = (lines[i].a->position.x+lines[i].b->position.x)/2.0;
				midpoint.y = (lines[i].a->position.y+lines[i].b->position.y)/2.0;
				//get the current length
				float len = sqrtf(square(abs(lines[i].a->position.x-lines[i].b->position.x)) + square(abs(lines[i].a->position.y-lines[i].b->position.y)));
				//get the direction
				struct vector direction;
				direction.x = (lines[i].a->position.x-lines[i].b->position.x)/len;
				direction.y = (lines[i].a->position.y-lines[i].b->position.y)/len;
				//only update the position of the points that arent locked
				if(!lines[i].a->locked){
					//set a position
					lines[i].a->position.x = direction.x*lines[i].length+midpoint.x;
					lines[i].a->position.y = direction.y*lines[i].length+midpoint.y;
				}
				if(!lines[i].b->locked){
					//set b position
					lines[i].b->position.x = (direction.x*lines[i].length*-1)+midpoint.x;
					lines[i].b->position.y = (direction.y*lines[i].length*-1)+midpoint.y;
				}
			}
		}
		//draw lines
		//bresenham's line drawing algorithm
		for(int i = 0; i < NUM_LINES; i++){
			if(lines[i].exists){
				int dX = (abs(lines[i].a->position.x-lines[i].b->position.x)); //the change in x
				int dY = (abs(lines[i].a->position.y-lines[i].b->position.y)); // the change in y
				if((dX > dY && lines[i].a->position.x > lines[i].b->position.x) || (dY > dX && lines[i].a->position.y > lines[i].b->position.y)){
					//we need to swap a and b
					struct point* tmp = lines[i].a;
					lines[i].a = lines[i].b;
					lines[i].b = tmp;
				}
				if(dX > dY){
					//we are mostly traveling along the x axis
					int A = 2*abs((lines[i].a->position.y)-(lines[i].b->position.y));
					int B = A-(2*dX);
					int P = A-(dX);
					int curY = lines[i].a->position.y;
					int dir = (lines[i].a->position.y < lines[i].b->position.y) ? 1:-1;
					for(int x = (lines[i].a->position.x)+1;
							x < lines[i].b->position.x && x > lines[i].a->position.x;
							x += 1){
						if(P < 0){
							P += A;
						}else{
							curY += dir;
							P += B;
						}
						mvprintw(curY, x, "o");
					}
				}else{
					//we are mostly traveling along the y axis, do the same thing but swap the x and y
					int A = 2*abs((lines[i].a->position.x)-(lines[i].b->position.x));
					int B = A-(2*dY);
					int P = A-(dY);
					int curX = lines[i].a->position.x;
					int dir = (lines[i].a->position.x < lines[i].b->position.x) ? 1:-1;
					for(int y = (lines[i].a->position.y)+1;
							y < lines[i].b->position.y && y > lines[i].a->position.y;
							y += 1){
						if(P < 0){
							P += A;
						}else{
							curX += dir;
							P += B;
						}
						mvprintw(y, curX, "o");
					}	
				}
			}
		}
		//draw points
		for(int i = 0; i < NUM_POINTS; i++){
			if(points[i].exists){
				mvprintw((int)points[i].position.y, (int)points[i].position.x, "0");
			}
		}
		refresh();
		getmaxyx(stdscr, maxRow, maxCol);
	}
	endwin();
}
