A rope physics simulator that runs in your terminal.

A mouse is required.

The physics simulation is made up of points and lines.

Points have a position, and a velocity, and can also be locked in place.

Lines connect 2 points together physically.

By default, there is a maximum of 50 points and 50 lines.

# Controls

q - quit

p - add point at mouse click location(will be locked in place by default)

u - toggle lock of point at mouse click location

U - unlock all the points in a rectangular area

l - add line between the next 2 clicked points

L - add points in sequence and automatically connects them with lines. press any key to stop adding points

v - update velocity of a point. Click on a point to select it, then click in the direction you want it to go.

d - delete a clicked point

# Compiling and Running

To compile:
```
make
```
To run:
```
./rope
```
